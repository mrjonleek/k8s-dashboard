Deploy the Kubernetes Dashboard resources
```bash
kubectl apply -k https://gitlab.com/mrjonleek/k8s-dashboard.git
```

Generate access token 

> `pbcopy` is a Mac OSX function only.

```bash
kubectl -n kubernetes-dashboard create token admin-user | pbcopy
```

Expose dashboard
```bash
kubectl proxy
```

[Dashboard](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/)
